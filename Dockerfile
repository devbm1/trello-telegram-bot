FROM python:3.6
ENV PYTHONUNBUFFERED 1
ENV C_FORCE_ROOT true

RUN mkdir -p /opt
COPY requirements.txt /opt/
WORKDIR /opt
RUN pip install -r requirements.txt
COPY .  /opt/
WORKDIR /opt/app/


CMD python startbot.py
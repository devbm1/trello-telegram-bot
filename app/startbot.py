import os
from ttb.telegram_bot.bot import TBot
from ttb.trelloClient.client import Trello
from messageBus import MessageBus

message_bus = MessageBus()
Trello(
    api_key=os.environ['TRELLO_KEY'],
    api_secret=os.environ['TRELLO_SECRET'],
    board_id=os.environ['TRELLO_BOARD'],
    list_id=os.environ['TRELLO_LIST'],
    message_bus=message_bus
).run()
TBot(token=os.environ['BOT_TOKEN'], message_bus=message_bus).run()

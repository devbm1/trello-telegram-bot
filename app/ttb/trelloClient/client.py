import requests
from io import BytesIO
from trello import TrelloClient
from messageBus import MESSAGE_TYPE, RESPONSE_TYPE


class Trello:
    def __init__(self, api_key=None, api_secret=None, message_bus=None, board_id=None, list_id=None):
        self.api_key = api_key
        self.api_secret = api_secret
        self.message_bus = message_bus
        self.board_id = board_id
        self.list_id = list_id

    def _init_client(self):
        self.client = TrelloClient(
            api_key=self.api_key,
            api_secret=self.api_secret,
            # token='your-oauth-token-key',
            # token_secret='your-oauth-token-secret'
        )

    def new_task(self, task_type, task):
        if task_type == MESSAGE_TYPE:
            card = self.add_card(task)
            self.message_bus.message(RESPONSE_TYPE, dict(card=card, message=task))

    def _init_message_bus_callbacks(self):
        self.message_bus.subscribe(MESSAGE_TYPE, self.new_task)

    def _start_actions(self):
        self.board = self.client.get_board(self.board_id)
        self.list = self.board.get_list(self.list_id)

    def add_card(self, message):
        card = self.list.add_card(
            message.task_text
        )
        for i, attach in enumerate(message.task_attachments):
            card.attach(name=attach.get('name', f'File{i+1}'), mimeType=attach.get('mime_type'), file=self.load_file(attach['url']))
        return card

    def load_file(self, url):
        response = requests.get(url)
        return BytesIO(response.content)


    def run(self):
        self._init_client()
        self._init_message_bus_callbacks()
        self._start_actions()

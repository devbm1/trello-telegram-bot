from telegram.ext import BaseFilter


class BotMentioned(BaseFilter):
    name = 'Filters.botDirect'

    def filter(self, message):
        text = message.text or message.caption or ''
        bot_name = f'@{message.bot.username}'
        return bot_name in text and (self.has_text(message) or self.has_attachments(message))

    def has_attachments(self, message):
        base_validator = bool(message.photo or message.document or message.video)
        if base_validator:
            return True
        if message.reply_to_message:
            return self.has_attachments(message.reply_to_message)

    def has_text(self, message):
        if message.text:
            text = message.text.replace(f'@{message.bot.username}', '').replace(' ', '')
            base_validator = len(text) > 3
            if base_validator:
                return True
        if message.reply_to_message:
            return self.has_text(message.reply_to_message)




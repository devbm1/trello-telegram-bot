import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram.utils.request import Request
from telegram import Bot

from .filters import BotMentioned
from messageBus import RESPONSE_TYPE, MESSAGE_TYPE

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


class TBot:

    def __init__(self, token, message_bus=None, *args, proxy=None, **kwargs):
        self.token = token
        self.proxy = proxy
        self.message_bus = message_bus
        self.updater = None
        self.bot = None

    def start(self, update, context):
        """Send a message when the command /start is issued."""
        update.message.reply_text('Hi!')
        pass

    def error(self, update, context):
        """Log Errors caused by Updates."""
        update.message.reply_text('Error!')
        logger.warning('Update "%s" caused error "%s"', update, context.error)

    def send_answer(self, data):
        url = data['card'].url
        chat_id = data['message'].chat.id
        message_id = data['message'].message_id
        self.bot.send_message(
            text=url,
            chat_id=chat_id,
            reply_to_message_id=message_id
        )

    def message(self, update, context):
        try:
            self.message_bus.message(MESSAGE_TYPE, self.enchance_message(update.message))
        except Exception as e:
            print(e)

    def enchance_message(self, message):
        message.task_text = self.get_text(message)
        message.task_attachments = self.get_attachments(message)
        return message

    def get_attachments(self, message):
        attachments = self.attachments_list(message)
        if message.reply_to_message:
            return self.get_attachments(message.reply_to_message)
        return attachments

    def attachments_list(self, message):
        attachments = []

        if message.photo:
            photo = message.photo[-1]
            file = photo.get_file(timeout=10)
            attachments.append(dict(name='img1.jpg', url=file.file_path, mime_type='image/jpeg'))

        if message.document:
            file = message.document.get_file()
            attachments.append(dict(url=file.file_path))
        if message.video:
            file = message.video.get_file()
            attachments.append(dict(url=file.file_path))
        return attachments

    def get_text(self, message):
        text = message.text and message.text.replace(f'@{message.bot.username}', '') or ''
        text += message.caption and message.caption.replace(f'@{message.bot.username}', '') or ''
        if message.reply_to_message:
            text += self.get_text(message.reply_to_message)
        return text

    def result_message(self, task_type, task):
        if task_type == RESPONSE_TYPE:
            self.send_answer(task)

    def _init_conn(self):
        kwargs = dict()

        if self.proxy:
            kwargs['request_kwargs'] = self.proxy

        updater = Updater(self.token, use_context=True, **kwargs)
        self.updater = updater
        self.bot = updater.bot

    def _init_handlers(self):
        # Get the dispatcher to register handlers
        dp = self.updater.dispatcher
        dp.add_handler(CommandHandler("start", self.start))
        dp.add_handler(MessageHandler(BotMentioned(), self.message))

        # log all errors
        dp.add_error_handler(self.error)

    def _init_message_buss_connects(self):
        if not self.message_bus:
            pass
        self.message_bus.subscribe(RESPONSE_TYPE, self.result_message)

    def _run_bot(self):
        self.updater.start_polling()
        self.updater.idle()

    def run(self):
        self._init_conn()
        self._init_handlers()
        self._init_message_buss_connects()
        self._run_bot()


from functools import wraps
import logging

logger = logging.getLogger(__name__)


MESSAGE_TYPE = 'MESSAGE'
RESPONSE_TYPE = 'RESPONSE'
TASK_TYPES = [MESSAGE_TYPE, RESPONSE_TYPE]


def validate_task_type(fn):
    @wraps(fn)
    def wrapped(*args, **kwargs):
        if args[1] not in TASK_TYPES:
            raise ValueError(f'task_type must be in: {" ".join(TASK_TYPES)}')
        return fn(*args, **kwargs)
    return wrapped


class MessageBus:
    def __init__(self):
        self.subscribers = {
            MESSAGE_TYPE: [],
            RESPONSE_TYPE: []
        }

    @validate_task_type
    def subscribe(self, task_type, callback):
        self.subscribers[task_type].append(callback)

    @validate_task_type
    def message(self, task_type, data):
        for handler in self.subscribers[task_type]:
            handler(task_type, data)



